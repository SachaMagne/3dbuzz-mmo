﻿using System;
using System.Runtime.Remoting.Messaging;
using BuzzMMo.Base.Infrastrure;

namespace  BuzzMMo.Base.Infrastructure
{
    public class ComponentMap
    {
        public MappedComponent[] Components { get; private set; }

        public ComponentMap()  {
            Components = new MappedComponent[byte.MaxValue];
        }

        public MappedComponent MapComponent(Type componentType, byte componentId) {
            if (Components[componentId] != null)
                throw new ArgumentException("Already mapped.", "componentId");

            var component = new MappedComponent(componentType, componentId);
            Components[componentId] = component;
            return component;
        } 
    }
}