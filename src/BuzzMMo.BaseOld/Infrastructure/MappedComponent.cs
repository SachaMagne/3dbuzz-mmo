﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuzzMMo.Base.Infrastrure
{
    class MappedComponent
    {
        public Type Type { get; private set; }
        public byte Id { get; private set; }

        public MappedComponent(Type type, byte id) {
            Type = type;
            Id = id;
        }
    }
}
