﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BuzzMMO.Base.Infrastructure;
using FluentAssertions;
using NUnit.Framework;

namespace BuzzMMO.Test.Base
{
    [TestFixture]
    public class SimpleSerialiserTests
    {
        [Test]
        public void CanReadAndWriteStrings()
        {
            const string testString = "Hello wolrd";
            var ser = new SimpleSerialiser();
            TestSerialiser(
                bw => ser.WriteObject(bw, typeof (string), testString),
                br => ser.ReadObject(br, typeof (string)).Should().Be(testString)
             );
        }
        [Test]
        public void CanReadAndWriteSGuid() {
            var guid = new Guid("{BABEBFA2-2A9D-4DDA-875A-4B3D8CC940E0}");
            var ser = new SimpleSerialiser();
            TestSerialiser(
                bw => ser.WriteObject(bw, typeof(Guid), guid),
                br => ser.ReadObject(br, typeof(Guid)).Should().Be(guid)
             );
        }

        [Test]
        public void CanReadAndWriteDateTime() {
            var datetime = new DateTime(1930, 12, 20);
            var ser = new SimpleSerialiser();
            TestSerialiser(
                bw => ser.WriteObject(bw, typeof(DateTime), datetime),
                br => ser.ReadObject(br, typeof(DateTime)).Should().Be(datetime)
             );
        }
        private void TestSerialiser( Action<BinaryWriter> writer, Action<BinaryReader> read) {
            byte[] bytes;

            using (var ms = new MemoryStream())
            using (var bw = new BinaryWriter(ms)) {
                writer(bw);
                bytes = ms.ToArray();
            }

            using (var ms = new MemoryStream(bytes))
            using (var br = new BinaryReader(ms))
                read(br);

        }
        


    }
}
