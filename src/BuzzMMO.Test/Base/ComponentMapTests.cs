﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BuzzMMO.Base.Infrastructure;
using NUnit.Framework;
using FluentAssertions;


namespace BuzzMMO.Test.Base
{
    [TestFixture]
    class ComponentMapTests
    {
        class TestAutoMapAssemblyAttribute : Attribute {
            
        }


        interface ITestComponents
        {}

        interface ITestComponent2
        {
            void Method();
            void Method2();
        }

        interface ITestComponent3 {
            void VoidMethodNoParam();
            void VoidMethodOneParams(string param);
            void VoidMethodTwoParams(bool param1, int param2);
        }

        [TestAutoMapAssemblyAttribute]
        interface ITestComponent4
        {
            void VoidMethodNoParam();
            void VoidMethodOneParams(string param);
            void VoidMethodTwoParams(bool param1, int param2);
        }

        [TestAutoMapAssembly]
        interface ITestComponent5 {
            
 
        }

        [TestAutoMapAssembly]
        interface ITestComponent6 {
        }

        interface ITestComponent7 {
            void NoResponse();
            IRpcResponse ResponseNoResult();
            IRpcResponse<int> ResponseWithResult();
        }

        class TestRpcResponse:IRpcResponse {
           }

        class TestRpcResponse<T>:IRpcResponse<T> {
        }

        class TestComponent7 : ITestComponent7{


            public IRpcResponse Response1 { get; set; }
            public IRpcResponse<int> Response2 { get; set; }


            public void NoResponse() { }

            public IRpcResponse ResponseNoResult() {
                return Response1;
            }

             public  IRpcResponse<int> ResponseWithResult() {
                return Response2;
            }
        }
        class TestComponent3 : ITestComponent3 {

            public int VoidMethodNoParamsCallCount { get; private set; }
            public int VoidMethodOneParamsCallCount { get; private set; }
            public int VoidMethodTwoParamsCallCount { get; private set; }

            public string VoidMethodOneParamParam1 { get; private set; }
            public bool VoidMethodTwoParamParam1 { get; private set; }
            public int VoidMethodTwoParamParam2 { get; private set; }

            public void VoidMethodNoParam() {
                VoidMethodNoParamsCallCount++;
            }

            public void VoidMethodOneParams(string param) {
                VoidMethodOneParamsCallCount++;
                VoidMethodOneParamParam1 = param;
            }

            public void VoidMethodTwoParams(bool param1, int param2) {
                VoidMethodTwoParamsCallCount++;
                VoidMethodTwoParamParam1 = param1;
                VoidMethodTwoParamParam2 = param2;
            }
        }

        
        [Test]
        [TestCase(typeof(ITestComponents),byte.MinValue)]
        [TestCase(typeof(ITestComponents),byte.MaxValue)]
        [TestCase(typeof(ITestComponents),128)]
        public void ManuallyMapsComponent(Type componentType, byte componentId) {
            var componentmap = new ComponentMap();
            componentmap.MapComponent(componentType, componentId);

            var mappedComponent = componentmap.Components[componentId];
            mappedComponent.Should().NotBeNull();
            mappedComponent.Type.Should().Be(componentType);
            mappedComponent.Id.Should().Be(componentId);
        }
    
        [Test]
        public void ShouldCreateAnExecptionError(){
            var componentmap = new ComponentMap();
           componentmap.MapComponent(typeof(ITestComponents), 12);
            // we test if we got the proper execpetion
            Action failed=() => componentmap.MapComponent(typeof(ITestComponents), 12);
            failed.ShouldThrow<ArgumentException>();

        }

        [Test]
        [TestCase(typeof(ITestComponents), byte.MinValue)]
        [TestCase(typeof(ITestComponents), byte.MaxValue)]
        [TestCase(typeof(ITestComponents), 128)]
        public void CanGetMappedComponentByType(Type componentType, byte componentId  )
        {
            var componentmap = new ComponentMap();
            componentmap.MapComponent(componentType, componentId);
            var mappedComponent = componentmap.GetComponent(componentType);
            mappedComponent.Should().NotBeNull();
            mappedComponent.Type.Should().Be(componentType);
            mappedComponent.Id.Should().Be(componentId);
        }

        [Test]
        public void MappingTwoCompoThrowExecption() {
            var componentmap = new ComponentMap();
            componentmap.MapComponent(typeof(ITestComponents), 12);
            Action failed = () => componentmap.MapComponent(typeof(ITestComponents), 13);
            failed.ShouldThrow<Exception>();
        }

        [Test]
        [TestCase(byte.MinValue)]
        [TestCase(byte.MaxValue)]
        [TestCase(1)]
        [TestCase(128)]
        public void ManuallyMapMethod2Component(byte id) {
            var componentmap = new ComponentMap();
            var component = componentmap.MapComponent(typeof (ITestComponent2), 4);
            
            var methodeInfo = typeof (ITestComponent2).GetMethod("Method");
            component.MapMethod(methodeInfo, id);

            var method = component.Methods[id];
            method.Id.Should().Be(id);
            method.Component.Should().Be(component);
            method.MethodInfo.Should().BeSameAs(methodeInfo);

        }

        [Test]
        public void MappingTwoMethodWithSameIdThrowsException() {
            var componentmap = new ComponentMap();
            var component = componentmap.MapComponent(typeof(ITestComponent2), 4);

            component.MapMethod(typeof (ITestComponent2).GetMethod("Method"), 1);
            Action failed = () => component.MapMethod(typeof(ITestComponent2).GetMethod("Method2"), 1);
            failed.ShouldThrow<Exception>();
        }

        [Test]
        public void MappingMethodRequiresMethodExistOnParent() {
            var componentmap = new ComponentMap();
            var component = componentmap.MapComponent(typeof(ITestComponents), 4);

            Action failed = () => component.MapMethod(typeof(ITestComponent2).GetMethod("Method2"), 5); 
        }

        [Test, TestCase(byte.MinValue), TestCase(byte.MaxValue), TestCase(1), TestCase(128)]
        public void CanGetMappedMethodViaMethodInfo(byte methodid) {
            var componentmap = new ComponentMap();
            var component = componentmap.MapComponent(typeof(ITestComponent2), 4);
            var methodinfo = typeof (ITestComponent2).GetMethod("Method");

            var method = component.MapMethod(methodinfo, methodid);
            component.GetMethod(methodinfo).Should().Be(method);

        }

        [Test]
        public void MappingTwoMethodsWithSameMethodInfoShoudThrowExecption() {
            var componentmap = new ComponentMap();
            var component = componentmap.MapComponent(typeof(ITestComponent2), 4);
            var methodinfo = typeof(ITestComponent2).GetMethod("Method");

            component.MapMethod(methodinfo, 4);
            Action failed = () => component.MapMethod(methodinfo, 5);
            failed.ShouldThrow<Exception>();

        }

        [Test]
        public void CanAccessMethodFromCompoMap() {
            var componentmap = new ComponentMap();
            var component = componentmap.MapComponent(typeof(ITestComponent2), 4);

           var mappedMethod= component.MapMethod(typeof (ITestComponent2).GetMethod("Method"), 5);

            componentmap.Methods[4][5].Should().BeSameAs(mappedMethod);


        }

        [Test]
        public void CanInvokeVoidMethodOnComponent() {
            var map = CreateTestComponent3Map();
            var componentObject = new TestComponent3();
            map.Methods[0][0].Invoke(componentObject,null);
            componentObject.VoidMethodNoParamsCallCount.Should().Be(1);
        }

        [Test]
        public void CanInvokeVoidMethodMultipleTimeOnComponent() {
            const int targetInvokeCount = 30;
            var map = CreateTestComponent3Map();
            var componentObject = new TestComponent3();
            for (int i = 0; i < targetInvokeCount; i++)                
                map.Methods[0][0].Invoke(componentObject, null);

            componentObject.VoidMethodNoParamsCallCount.Should().Be(targetInvokeCount);
           

        }


        [Test]
        public void CanInvokeVoidMethOneParamOnComponent() {
            const string param1 = "hello world";

            var map = CreateTestComponent3Map();
            var componentObject = new TestComponent3();

            map.Methods[0][1].Invoke(componentObject, new object[] {param1});
            componentObject.VoidMethodOneParamsCallCount.Should().Be(1);
            componentObject.VoidMethodOneParamParam1.Should().Be(param1);
        }

        [Test]
        public void ReservedComponentIdLimitPreventCompoFromBeingMapped() {
            var map = new ComponentMap(3);
            Action action1 = () => map.MapComponent(typeof (ITestComponents), 0);
            Action action2 = () => map.MapComponent(typeof(ITestComponents), 1);
            Action action3 = () => map.MapComponent(typeof(ITestComponents), 2);

            action1.ShouldThrow<ArgumentException>();
            action2.ShouldThrow<ArgumentException>();
            action3.ShouldThrow<ArgumentException>();
            map.MapComponent(typeof (ITestComponents), 3);
        }


        [Test]
        public void CanInvokeVoidMethodTwoParamOnComponent() {
            const bool param1 = true;
            const int param2 = 12345;

            var map = CreateTestComponent3Map();
            var componentObject = new TestComponent3();

            map.Methods[0][2].Invoke(componentObject, new object[] { param1,param2 }); // 2 is the second method mapped down there !!!
            componentObject.VoidMethodTwoParamsCallCount.Should().Be(1);
            componentObject.VoidMethodTwoParamParam1.Should().Be(param1);
            componentObject.VoidMethodTwoParamParam2.Should().Be(param2);
        }

        [Test]
        public void ReturnValuesDoGetPassedBackFromMappedMethod() {
            var map = new ComponentMap();
            var component = map.MapComponent(typeof (ITestComponent7), 0);
            component.MapMethod(typeof (ITestComponent7).GetMethod("ResponseNoResult"), 0);
            var obj = new TestComponent7();
            obj.Response1 = new TestRpcResponse();
            obj.Response2 = new TestRpcResponse<int>();
            var result = map.Methods[0][0].Invoke(obj, new object[0]);

            result.Should().BeSameAs(obj.Response1);


        }

        [Test]
        public void ReturnValueWithResultSeePrevTest() {
            var map = new ComponentMap();
            var component = map.MapComponent(typeof(ITestComponent7), 0);
            component.MapMethod(typeof(ITestComponent7).GetMethod("ResponseWithResult"), 0);
            var obj = new TestComponent7 {
                Response1 = new TestRpcResponse(),
                Response2 = new TestRpcResponse<int>()
            };

            var result = map.Methods[0][0].Invoke(obj, new object[0]);
            result.Should().BeSameAs(obj.Response2);

        }




        [Test]
        public void CanAutoMapComponent() {
            var map = new ComponentMap();
            map.AutoMapComponent(typeof (ITestComponent4));

            map.Components[0].Type.Should().Be(typeof (ITestComponent4));
            map.Components[0].Methods[0].MethodInfo.Name.Should().Be("VoidMethodNoParam");
            map.Components[0].Methods[1].MethodInfo.Name.Should().Be("VoidMethodOneParams"); 
            map.Components[0].Methods[2].MethodInfo.Name.Should().Be("VoidMethodTwoParams");


        }

        [Test]
        public void AutoMappingRespectReservedId() {
            var map = new ComponentMap(3);
            map.AutoMapComponent(typeof(ITestComponent4));

            map.Components[0].Should().Be(null);
            map.Components[1].Should().Be(null);
            map.Components[2].Should().Be(null);
            map.Components[3].Methods[0].MethodInfo.Name.Should().Be("VoidMethodNoParam");
            map.Components[3].Methods[1].MethodInfo.Name.Should().Be("VoidMethodOneParams");
            map.Components[3].Methods[2].MethodInfo.Name.Should().Be("VoidMethodTwoParams");



        }

        [Test]
        public void AutoMapAssemblySelf() {
            var map = new ComponentMap();
            map.AutoMapAssembly(typeof(ComponentMapTests).Assembly,typeof(TestAutoMapAssemblyAttribute));
            map.GetComponent(typeof (ITestComponent4)).Should().NotBeNull();
            map.GetComponent(typeof(ITestComponent5)).Should().NotBeNull();
            map.GetComponent(typeof(ITestComponent6)).Should().NotBeNull();

            Action failed = () => map.GetComponent(typeof (ITestComponents));
            failed.ShouldThrow<Exception>();

        }
        [Test]
        public void MappedMethodIdentifiesVoidReturnType()
        {
            var map = new ComponentMap();
            var component = map.MapComponent(typeof(ITestComponent7), 0);
            component.MapMethod(typeof(ITestComponent7).GetMethod("NoResponse"), 0);
            component.MapMethod(typeof(ITestComponent7).GetMethod("ResponseNoResult"), 1);
            component.MapMethod(typeof(ITestComponent7).GetMethod("ResponseWithResult"), 2);

            map.Methods[0][0].ReturnType.Should().Be(MappedMethodReturnType.Void);
            map.Methods[0][0].ResultType.Should().BeNull();
        }



        [Test]
        public void MappedMethodIdentifiesIRpcResponseReturnType() {
            var map = new ComponentMap();
            var component = map.MapComponent(typeof (ITestComponent7), 0);
            component.MapMethod(typeof (ITestComponent7).GetMethod("NoResponse"), 0);
            component.MapMethod(typeof(ITestComponent7).GetMethod("ResponseNoResult"), 1);
            component.MapMethod(typeof(ITestComponent7).GetMethod("ResponseWithResult"), 2);

            map.Methods[0][1].ReturnType.Should().Be(MappedMethodReturnType.Response);
            map.Methods[0][1].ResultType.Should().BeNull();
        }

        [Test]
        public void MappedMethodIdentifiesIRpcResponseReturnTypeWithResult()
        {
            var map = new ComponentMap();
            var component = map.MapComponent(typeof(ITestComponent7), 0);
            component.MapMethod(typeof(ITestComponent7).GetMethod("NoResponse"), 0);
            component.MapMethod(typeof(ITestComponent7).GetMethod("ResponseNoResult"), 1);
            component.MapMethod(typeof(ITestComponent7).GetMethod("ResponseWithResult"), 2);

            map.Methods[0][2].ReturnType.Should().Be(MappedMethodReturnType.ResponseWithResult);
            map.Methods[0][2].ResultType.Should().Be(typeof (int));
        }

        private ComponentMap CreateTestComponent3Map() {
            var componentMap = new ComponentMap();
            var component = componentMap.MapComponent(typeof (ITestComponent3), 0);
            component.MapMethod(typeof (ITestComponent3).GetMethod("VoidMethodNoParam"), 0);
            component.MapMethod(typeof(ITestComponent3).GetMethod("VoidMethodOneParams"), 1);
            component.MapMethod(typeof(ITestComponent3).GetMethod("VoidMethodTwoParams"), 2);
            return componentMap;
        }



    }
}
