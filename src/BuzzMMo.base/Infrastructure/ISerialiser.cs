﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BuzzMMO.Base.Infrastructure {
    public interface ISerialiser {
        void WriteArguments(BinaryWriter writer, IEnumerable<Type> types, IEnumerable<object> arguments);
        object[] ReadArguments(BinaryReader reader, IEnumerable<Type> types);

        void WriteObject(BinaryWriter writer, Type type, object value);
        object ReadObject(BinaryReader reader, Type type);

    }
}