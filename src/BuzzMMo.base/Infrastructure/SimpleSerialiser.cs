﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace BuzzMMO.Base.Infrastructure {
    public class SimpleSerialiser: ISerialiser
    {
        public void WriteArguments(BinaryWriter writer, IEnumerable<Type> types, IEnumerable<object> arguments) {
            using (var typeEnumerator = types.GetEnumerator())
            using (var argumentEnumerator = arguments.GetEnumerator()) {
                while (typeEnumerator.MoveNext() && argumentEnumerator.MoveNext()) {
                    WriteObject(writer, typeEnumerator.Current, argumentEnumerator.Current);
                }
            }

        }

        public object[] ReadArguments(BinaryReader reader, IEnumerable<Type> types) {
// ReSharper disable  PossibleMultipleEnumeration
            var returnValue = new object[types.Count()];
            var index = 0;
            foreach (var type in types) {
                returnValue[index++] = ReadObject(reader, type);
            }
            return returnValue;
        }

        public void WriteObject(BinaryWriter writer, Type type, object value) {
            WriteSimple(writer, type, value);
        }

        public object ReadObject(BinaryReader reader, Type type) {
            return ReadSimple(reader, type);
        }

        private void WriteSimple(BinaryWriter writer, Type type , Object value) {
            if (type == typeof (uint))
                writer.Write((uint) value);
            else if (type == typeof (byte))
                writer.Write((byte) value);
         else if (type == typeof (string))
                writer.Write((string) value);
         else if (type == typeof (float))
                writer.Write((float) value);
         else if (type == typeof (double))
                writer.Write((double) value);
         else if (type == typeof (decimal))
                writer.Write((decimal) value);
         else if (type == typeof (int))
                writer.Write((int) value);
        else if (type == typeof (short))
                writer.Write((short) value);
         else if (type == typeof (ushort))
                writer.Write((ushort) value);
         else if (type == typeof (long))
                writer.Write((long) value);
         else if (type == typeof (ulong))
                writer.Write((ulong) value);
         else if (type == typeof (char))
                writer.Write((char) value);
         else if (type == typeof (bool))
                writer.Write((bool) value);

         else if (type == typeof (Guid))
                writer.Write(((Guid) value).ToByteArray());
         else if (type == typeof (DateTime))
             writer.Write(((DateTime) value).Ticks);
         else
             throw new ArgumentException(string.Format("Cannot write {0}", type.FullName), "value");

        }

        private object ReadSimple(BinaryReader reader, Type type) {
            if (type == typeof (uint))
              return reader.ReadUInt32();

            if (type == typeof (byte))
                return reader.ReadByte();

            if (type == typeof (string))
               return reader.ReadString();

            if (type == typeof (float))
                return reader.ReadSingle();

            if (type == typeof (double))
                return reader.ReadDouble();

            if (type == typeof (decimal))
                return reader.ReadDecimal();

            if (type == typeof (int))
                return reader.ReadInt32();

            if (type == typeof (short))
                return reader.ReadInt16();

            if (type == typeof (ushort))
                return reader.ReadInt16();

            if (type == typeof (long))
                return reader.ReadInt64();

            if (type == typeof (ulong))
                return reader.ReadUInt64();

            if (type == typeof (char))
                return reader.ReadChar();

            if (type == typeof (bool))
                return reader.ReadBoolean();

            if (type == typeof (Guid))
                return new Guid(reader.ReadBytes(16));

            if (type == typeof (DateTime))
                return new DateTime(reader.ReadInt64());
            throw new ArgumentException(string.Format("Cannot read {0}", type.FullName), "type");
        }
    }
}