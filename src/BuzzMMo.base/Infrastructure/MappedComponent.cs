﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Configuration;
using System.Reflection;

namespace BuzzMMO.Base.Infrastructure
{
    public class MappedComponent
    {
        private readonly Dictionary<MethodInfo, MappedMethod> _methodInfosToMethods;

        private byte _nextAutoMappedMethodId;

        public Type Type { get; private set; }
        public byte Id { get; private set; }
        public MappedMethod[] Methods { get; private set; }

        public int MethodCounts { get { return _methodInfosToMethods.Count; } }
        public IEnumerable<MappedMethod> AllMethods { get { return _methodInfosToMethods.Values; } } 

        public MappedComponent(Type type, byte id) {
            Type = type;
            Id = id;
            _nextAutoMappedMethodId = 0;
            Methods = new MappedMethod[byte.MaxValue + 1];
            _methodInfosToMethods = new Dictionary<MethodInfo, MappedMethod>();
        }


        public MappedMethod MapMethod(MethodInfo methodInfo, Byte id) {
            if (Methods[id] != null)
                throw new ArgumentException( "Method already implemented","id");

            if (methodInfo.DeclaringType != Type)
                throw new ArgumentException("Method must be declared on component type", "methodInfo");

            
            var method = new MappedMethod(this, methodInfo, id);
            _methodInfosToMethods.Add(methodInfo, method);
            Methods[id] = method;
            return method;
        }

        public MappedMethod GetMethod(MethodInfo method) {
            return _methodInfosToMethods[method];
        }

        public void AutoMapMethods() {
            foreach (var method in Type.GetMethods(BindingFlags.Instance | BindingFlags.Public)) {
                MapMethod(method, _nextAutoMappedMethodId);
                _nextAutoMappedMethodId++;
            }
        }
    
    }
}
