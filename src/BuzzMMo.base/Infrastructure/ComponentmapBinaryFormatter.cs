﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BuzzMMO.Base.Infrastructure.Extensions;

namespace BuzzMMO.Base.Infrastructure
{
   public  class ComponentMapBinaryFormatter {
        public ComponentMap Load(BinaryReader reader) {
            var map = new ComponentMap(reader.ReadByte());
            var componentCount = reader.ReadInt32();

            for (var i = 0; i < componentCount; i++) {
                var componentId = reader.ReadByte();
                var componentType = Type.GetType(reader.ReadString());
                var component = map.MapComponent(componentType,componentId);

                var methodCount = reader.ReadInt32();
                for (var j = 0; j < methodCount; j++) {
                    var methodId = reader.ReadByte();
                    var methodInfo = componentType.GetMethodBySignature(reader.ReadString());
                    component.MapMethod(methodInfo, methodId);
                }
            }
            return map;
        }

        public void Save(BinaryWriter writer, ComponentMap map) {
            writer.Write(map.ReservedComponentIdLimit);
            writer.Write(map.ComponentCount);
            foreach (var compo in map.AllComponents) {
                writer.Write(compo.Id);
                writer.Write(compo.Type.AssemblyQualifiedName);
                writer.Write(compo.MethodCounts);
                foreach (var method in compo.AllMethods) {
                    writer.Write(method.Id);
                    writer.Write(method.MethodInfo.GetMethodSignature());
                }
            }
        }
    }
}
