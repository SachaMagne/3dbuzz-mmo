﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace  BuzzMMO.Base.Infrastructure
{
    /// <summary>
    /// Allows us to exchange data and methods between clients and servers 
    /// without the need to update each side without doing reflection and waste bandwith.
    /// 
    /// </summary>
    public class ComponentMap
    {
        
        public MappedComponent[] Components { get; private set; }
        public MappedMethod[][] Methods { get; private set; }
        public byte ReservedComponentIdLimit { get; private set; }



        private readonly Dictionary<Type, MappedComponent> _typeToComponents;
        private byte _nextAutoMappedComponentId;

        public IEnumerable<MappedComponent> AllComponents { get { return _typeToComponents.Values; } }
        public int ComponentCount { get { return _typeToComponents.Count; } }

 


        public ComponentMap( ) : this(0) {}

        public ComponentMap(byte reservedComponentLimit ) {
            ReservedComponentIdLimit = reservedComponentLimit;
                
            _typeToComponents = new Dictionary<Type, MappedComponent>();

            Components = new MappedComponent[byte.MaxValue+1];
            Methods = new MappedMethod[byte.MaxValue+1][];

            _nextAutoMappedComponentId =  reservedComponentLimit;

        }

        public MappedComponent MapComponent(Type componentType, byte componentId) {
            if (Components[componentId] != null)
                throw new ArgumentException("ERR - Already mapped.", "componentId");

            if (componentId < ReservedComponentIdLimit)
                throw new ArgumentException("ERR -ID passes reserved component id limit", "componentId");
                                                                                  

            // Should crash if we mapped the same compo twice byu design.
            var component = new MappedComponent(componentType, componentId);
            _typeToComponents.Add(componentType, component);
            
            Components[componentId] = component;
            Methods[componentId] = component.Methods;
            return component;
        }

        public MappedComponent GetComponent(Type componentType) {
            return _typeToComponents[componentType];
        }

        public MappedComponent AutoMapComponent(Type componentType) {
            var mappedComponent = MapComponent(componentType, _nextAutoMappedComponentId);
            _nextAutoMappedComponentId++;
            mappedComponent.AutoMapMethods();
            return mappedComponent;
        }

        public void AutoMapAssembly(Assembly assemblyToMap, Type attributeSelector) {
            foreach (var type in assemblyToMap.GetTypes()) {
                if (type.GetCustomAttributes(attributeSelector, false).Any())
                    AutoMapComponent(type);
            }
        }

        public void AutoMapCurrentAppDomain(Type attributeSelector) {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
                AutoMapAssembly(assembly,attributeSelector);
            }
        }
    }
}