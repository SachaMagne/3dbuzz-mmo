﻿namespace BuzzMMO.Base.Infrastructure {
    public enum MappedMethodReturnType {
        Void,
        Response,
        ResponseWithResult
    }
}